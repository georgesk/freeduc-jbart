#! /bin/sh

if [ -f /home/user/.config/localtime-stamp ] ; then
    exit 0
else
    touch /home/user/.config/localtime-stamp
    sudo ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
fi

