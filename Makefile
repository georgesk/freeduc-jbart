TARGET = live-freeduc
GIVEBACK_DIR = /srv/ftp/freeduc-usb/freeduc-jbart/24.03

default:
	echo "possible targets: 'stick', 'rsync_to_freeduc' 'rsync_from_freeduc'"

stick:
	sudo live-clone

STICKDEVICE = $(shell lsblk| grep persistence | sed -n 's/.*\(sd.\)[0-9].*/\1/ p')
LIVE_IMAGES = $(shell ls live-image*)

image_16BG: live-image-amd64.stick16G.img.gz

live-image-amd64.stick16G.img.gz:
	@if [ -n "$(STICKDEVICE)" ]; then \
	  echo "writing to live-image-amd64.stick16G.img.gz" ; \
	  sudo bar -if /dev/$(STICKDEVICE) | gzip -c9 > $@; \
	else \
	  echo "no device was found with a partition named 'persistence'"; \
	fi

sum: sha512sums.txt

sha512sums.txt: $(LIVE_IMAGES)
	sha512sum live-image* > $@

sign: sha512sums.txt.asc

sha512sums.txt.asc: sha512sums.txt
	gpg -sab sha512sums.txt

giveback:
	rsync -v --progress \
	  sha512sums.txt* live-image-* \
	  freeduc.science:$(GIVEBACK_DIR)

rsync_to_freeduc:
	ssh freeduc.science mkdir -p jbart/$(TARGET)
	rsync -av --exclude="live-image*" --exclude "*.log" --delete auto config freeduc.science:jbart/$(TARGET)/

rsync_from_freeduc:
	rsync -av freeduc.science:jbart/$(TARGET)/build.log .
	# get the built files of interest, except the source archives
	rsync -av --exclude "*source*" --exclude "*stick*" --progress \
	          freeduc.science:jbart/$(TARGET)/live-image* .

.PHONY: default stick rsync_to_freeduc rsync_from_freeduc image_16BG sums sign giveback
