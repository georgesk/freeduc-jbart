FREEDUC-JBART
=============

L'image USB vive [Freeduc-Jbart](ftp://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.fr.html)
est réalisée depuis l'année 2019 à l'aide du paquet **live-build**
de Raphaël Hertzog et Daniel Baumann. Auparavant, le CDROM, puis
les images USB vives du projet [Freeduc](https://fr.wikipedia.org/wiki/Freeduc)
étaient basées sur la distribution vive [KNOPPIX](http://www.knopper.net/knoppix/)

Ce dépôt contient le travail réalisé pour l'enseignement des sciences et
et de la programmation au [lycée Jean Bart](https://www.lyceejeanbart.fr)
à Dunkerque.

Versions utilisables
--------------------

| Désignation |   Tag       |    Branche |   Commentaires |
| ----------- | ----------- | ---------- | ---------------------------------------------------------- |
| 2019.06     |    v2019.06 |   cinnamon-jbart | Version utilisable au lycée Jean Bart pour les enseignements SNT | NSI et ISN ; aussi utilisée pour la formation des enseignants NSI. |
| 2024.1      |   v2024.01  | freeduc-jbart    | Version en développement |


